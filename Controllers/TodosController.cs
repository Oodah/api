﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using LiteDB.Wrapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly CollectionReference<Todo> TodoCollection;
        /// <summary>
        /// 
        /// </summary>
        public TodosController(CollectionReference<Todo> reference) => TodoCollection = reference;

        /// <summary>
        /// 
        /// </summary>
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// 
        /// </summary>
        [Authorize]
        [HttpGet, Route("get")]
        public IActionResult GetTodo(string id)
        {
            try
            {
                Todo _model = TodoCollection.Get(Guid.Parse(id));
                return Ok(_model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Authorize]
        [HttpGet, Route("get/all")]
        public IActionResult GetTodos(int offset, int rows)
        {
            try
            {
                (IList<Todo> list, long totalRows) = TodoCollection.GetPaged(
                    new PageOptions(offset, rows),
                    new SortOptions(SortOptions.Order.ASC, "DateAdded"));
                return Ok(new { list, totalRows});
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Authorize]
        [HttpPost, Route("add")]
        public async Task<IActionResult> CreateTodo(Todo model)
        {
            try
            {
                model.TodoId = Guid.NewGuid();
                TodoCollection.Insert(model);
                await TodoCollection.Commit();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Authorize]
        [HttpPut, Route("edit")]
        public async Task<IActionResult> ModifyTodo(Todo model)
        {
            try
            {
                if (model.TodoId.Equals(Guid.Empty))
                {
                    return BadRequest("TodoId is required.");
                }

                TodoCollection.Update(model);
                await TodoCollection.Commit();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Authorize]
        [HttpPut, Route("complete")]
        public async Task<IActionResult> CompleteTodo([FromQuery]string id)
        {
            try
            {
                Todo _todo = TodoCollection.Get(Guid.Parse(id));
                _todo.IsCompleted = true;

                TodoCollection.Update(_todo);
                await TodoCollection.Commit();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Authorize]
        [HttpDelete, Route("delete")]
        public async Task<IActionResult> RemoveTodo([FromQuery]string id)
        {
            try
            {
                 TodoCollection.Remove(Guid.Parse(id));
                await TodoCollection.Commit();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
