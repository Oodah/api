﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiteDB;
using API.Models;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Todo
    {
        /// <summary>
        /// 
        /// </summary>
        [BsonId]
        public Guid TodoId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        [EmailAddress]
        public string AddedBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public DateTime TodoDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsCompleted { get; set; }
              /// <summary>
              /// 
              /// </summary>
        public DateTime DateAdded { get; set; }
    }
}
